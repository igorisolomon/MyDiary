let firstTime = true;

// Display signUp page
const signUpPage = () =>{
    // mobile first
    if(window.screen.width<700){
        const sign = document.querySelector(".sign");
        if(firstTime){
            sign.classList.add("hide");     // hide sign

            firstTime = false;    // set firstTime to false
        }
        else{
            // hide mobileBtn
            const mobBtn = document.querySelector(".mobileBtn");
            mobBtn.classList.add("hide");

            sign.classList.remove("hide");     // hide sign
            sign.classList.add("show");     // show sign
        }
    }

    // hide sign in
    const signIn = document.querySelector("#in");
    signIn.className = "hide";

    // display sign up
    const signUp = document.querySelector("#up");
    signUp.className = "show";

    // style top button
    const upBtn = document.querySelector("#signUpBtn");
    const inBtn = document.querySelector("#signInBtn");

    inBtn.classList.remove("activeBtn");
    upBtn.classList.add("activeBtn");
    
    
    
}

// Display signIn page
const signInPage = () =>{
    // mobile first
    if(window.screen.width<700){
        const sign = document.querySelector(".sign");
        
        // hide mobileBtn
        const mobBtn = document.querySelector(".mobileBtn");
        mobBtn.classList.add("hide");

        sign.classList.remove("hide");     // hide sign
        sign.classList.add("show");     // show sign
    }
    
    // hide sign up
    const signUp = document.querySelector("#up");
    signUp.className = "hide";

    // display sign in
    const signIn = document.querySelector("#in");
    signIn.className = "show";

    // style top button
    // mobile first
    const upMob = document.querySelector("#signUpMob");
    const inMob = document.querySelector("#signInMob");
    // larger screen
    const upBtn = document.querySelector("#signUpBtn");
    const inBtn = document.querySelector("#signInBtn");

    upBtn.classList.remove("activeBtn");
    inBtn.classList.add("activeBtn");
}

// SignUp page on start
signUpPage();

// event listener for button
// mobile first
const upMob = document.querySelector("#signUpMob");
const inMob = document.querySelector("#signInMob");

upMob.addEventListener("click",signUpPage);
inMob.addEventListener("click",signInPage);

const upBtn = document.querySelector("#signUpBtn");
const inBtn = document.querySelector("#signInBtn");

upBtn.addEventListener("click",signUpPage);
inBtn.addEventListener("click",signInPage);