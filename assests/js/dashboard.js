const date = new Date('July 12, 2018');;    // get date
const day = date.getDate();
const todayMonth = date.getMonth();
const todayYear = date.getFullYear();

// Went designing the back-end, this month will only be filled w ith the months inputed previously
const months = ["January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


// setting today's month and year in DOM
const domDate = document.querySelector(".todayDate");
month = months[todayMonth];

let todayDate = `${month} ${todayYear}`;
domDate.innerHTML = todayDate;

// month in the list
let listMonth = document.querySelector(".month");
listMonth.innerHTML = month;

/**
 * The list,
 * Which will be populated dynamically from the data structure backend
 * by will first hold some sample to render to DOM
 */
// create ul and add class
let list = document.createElement("ul");
list.className = "list";
// dummy text
const dummyTemp = [[2,"content summary"],[3,"Beautiful day, I found the bug at last"],[5,"I almost had a girlfriend today ...lol"]];
dummyTemp.forEach(item =>{
    let day = item[0];    // get day
    let summary = item[1];    // get summary
    // the list 
    let dummyItem = document.createElement("li");    // create li class listItem
    dummyItem.className = "listItem";

    // this div hold the date of the content
    let dateNumItem = document.createElement("div");   // create div class dateNum
    dateNumItem.className = "dateNum";
    let dateNumItemText = document.createTextNode(day);     // create a text node and add day
    dateNumItem.appendChild(dateNumItemText);

    // this div hold the summaryt of the content
    let conSumItem = document.createElement("div");   // create div class conSum
    conSumItem.className = "conSum";
    let conSumItemText = document.createTextNode(summary);
    conSumItem.appendChild(conSumItemText);

    // add both div to li
    dummyItem.appendChild(dateNumItem);
    dummyItem.appendChild(conSumItem);

    // add li to ul
    list.appendChild(dummyItem);
})
// display list to DOM
const listing = document.querySelector(".listing");
listing.appendChild(list);



