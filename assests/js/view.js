const dummyContent = ["I almost got a girlfriend today ..lol", "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Reprehenderit voluptates porro possimus quisquam sed ducimus. Minus id non, explicabo tempore hic animi sint, quaerat dignissimos quam laudantium placeat enim dolore."]

const date = new Date('July 12, 2018');    // get date
const day = date.toDateString();

// the date
const domDate = document.querySelector(".date");    // get date div
let dateElement = document.createTextNode(day);    // get text element
domDate.appendChild(dateElement);    // append

//summary aspect
const summary = document.querySelector(".summary");
let summaryText = document.createTextNode(dummyContent[0]);
summary.appendChild(summaryText);

// content aspect
const content = document.querySelector(".content");
let contentText = document.createTextNode(dummyContent[1]);
content.appendChild(contentText);